# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180627134741) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pgcrypto"

  create_table "current_stocks", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "stock", null: false
    t.string "store_name", null: false
    t.string "shoe_name", null: false
    t.uuid "sales_log_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_name", "shoe_name"], name: "index_current_stocks_on_store_name_and_shoe_name", unique: true
  end

  create_table "sales_logs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "stock", null: false
    t.string "store_name", null: false
    t.string "shoe_name", null: false
    t.integer "stock_diff", null: false
    t.integer "balance_diff", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_name", "shoe_name"], name: "index_sales_logs_on_store_name_and_shoe_name"
  end

  create_table "shoe_current_prices", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "shoe_name", null: false
    t.float "selling_price", null: false
    t.float "buying_price", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shoe_name"], name: "index_shoe_current_prices_on_shoe_name", unique: true
  end

  add_foreign_key "current_stocks", "sales_logs"
end
