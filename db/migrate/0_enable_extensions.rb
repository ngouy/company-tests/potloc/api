class EnableExtensions < ActiveRecord::Migration[5.1]
  def up
    enable_extension 'pgcrypto'
  end
end
