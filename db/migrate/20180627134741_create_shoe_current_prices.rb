class CreateShoeCurrentPrices < ActiveRecord::Migration[5.1]
  def change
    create_table :shoe_current_prices, id: :uuid do |t|
      t.string :shoe_name,     null: false, unique: true
      t.float  :selling_price, null: false
      t.float  :buying_price,  null: false
      t.timestamps
    end

    add_index :shoe_current_prices, :shoe_name, unique: true
  end
end
