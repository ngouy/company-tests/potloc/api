class CreateSalesLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :sales_logs, id: :uuid do |t|
      t.integer :stock,        null: false
      t.string  :store_name,   null: false
      t.string  :shoe_name,    null: false
      t.integer :stock_diff,   null: false
      t.integer :balance_diff, null: false

      t.timestamps
    end

    create_table :current_stocks, id: :uuid do |t|
      t.integer :stock,        null: false
      t.string  :store_name,   null: false
      t.string  :shoe_name,    null: false
      t.uuid    :sales_log_id, null: false

      t.timestamps
    end

    add_index :sales_logs,     %i[store_name shoe_name]
    add_index :current_stocks, %i[store_name shoe_name], unique: true
    add_foreign_key :current_stocks, :sales_logs
  end
end
