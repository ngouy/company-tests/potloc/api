### Imported from my actual company api project
### Developped by redpist and ngouy (me)
### Gem will be released soon

module JsonApi
  module ControllerMixin
    module Base
      def self.included(base)
        base.class_variable_set(:@@model_klass, base.controller_path.split('/').last.classify.safe_constantize)
        base.class_variable_set(:@@model_filters, {}) if base.class_variable_get(:@@model_klass)
        base.class_variable_set(:@@model_filters_defaults, {}) if base.class_variable_get(:@@model_klass)
        base.class_variable_set(:@@model_sorts, {}) if base.class_variable_get(:@@model_klass)
        base.class_variable_set(:@@model_sorts_defaults, {}) if base.class_variable_get(:@@model_klass)
        base.class_variable_set(:@@model_included, []) if base.class_variable_get(:@@model_klass)
        base.include(JsonApi::ControllerMixin::InstanceMethods)
        base.extend(JsonApi::ControllerMixin::ClassMethods)
        base.primary_key = :id
      end
    end

    module Abstract
      def self.included(base)
        base.include(JsonApi::ControllerMixin::InstanceMethods)
        base.extend(JsonApi::ControllerMixin::ClassMethods)
      end
    end

    module ClassMethods

      attr_accessor :primary_key

      def column_filter(filter_name)
        model_filters[filter_name] = Proc.new {|records, filter_value|
          records.where(filter_name => filter_value.split(','))
        }
      end

      def column_sort(sort_name, nulls_last)
        model_sorts[sort_name] = Proc.new {|records, direction|
          records.order("#{model_klass.table_name}.#{sort_name} #{direction.to_s.upcase} NULLS #{nulls_last ? 'LAST' : 'FIRST'}")
        }
      end

      def custom_filter(filter_name, apply)
        model_filters[filter_name] = apply
      end

      def custom_sort(sort_name, apply)
        model_sorts[sort_name] = apply
      end

      def paginator(bool)
        self.class_variable_set(:@@paginator, bool);
        # if bool
        # end
      end

      def filter(filter_name, apply: nil, default: nil)
        column_names = model_klass.column_names.map(&:to_sym) if defined?(model_klass.column_names)
        model_filters_defaults[filter_name.to_s] = default if default
        if apply.class == Proc
          custom_filter(filter_name, apply)
        elsif defined?(model_klass.column_names) && ([filter_name] & column_names == [filter_name])
          column_filter(filter_name)
        else
          raise NoMethodError.new("Invalid filter :#{filter_name} for #{self}, you can define custom filters with the :apply parameter")
        end
      end

      def sorts_by(*sort_names)
        sort_names.each { |sort_name| sort_by(sort_name) }
      end

      def sort_by(sort_name, apply: nil, default: nil, nulls_last: false)
        column_names = model_klass.column_names.map(&:to_sym) if defined?(model_klass.column_names)
        model_sorts_defaults[sort_name.to_s] = default if default
        if apply.class == Proc
          custom_sort(sort_name, apply)
        elsif defined?(model_klass.column_names) && ([sort_name] & column_names == [sort_name])
          column_sort(sort_name, nulls_last)
        else
          raise NoMethodError.new("Invalid sort :#{sort_name} for #{self}, you can define custom sort with the :apply parameter")
        end
      end

      def always_include(*fields)
        class_variable_set(:@@model_included, model_included | fields)
      end

      def filters(*filters)
        column_names = model_klass.column_names.map(&:to_sym)
        if (filters & column_names != filters)
          (filters - column_names & filters).each do |filter_name|
            raise NoMethodError.new("Invalid filter :#{filter_name} for #{self}, you can define custom filters with the :apply parameter")
          end
        end
        (filters & column_names).each do |filter|
          self.column_filter(filter)
        end
      end

      def sorts(*sorts)
        column_names = model_klass.column_names.map(&:to_sym)
        if (sorts & column_names != sorts)
          (sorts - column_names & sorts).each do |sort_name|
            raise NoMethodError.new("Invalid sort :#{sort_name} for #{self}, you can define custom sorts with the :apply parameter")
          end
        end
        (sorts & column_names).each do |sort|
          self.column_sort(sort)
        end
      end

      def model_klass
        class_variable_get(:@@model_klass)
      end

      def model_filters
        class_variable_get(:@@model_filters)
      end

      def model_sorts
        class_variable_get(:@@model_sorts)
      end

      def model_filters_defaults
        class_variable_get(:@@model_filters_defaults)
      end

      def model_sorts_defaults
        class_variable_get(:@@model_sorts_defaults)
      end

      def model_included
        class_variable_get(:@@model_included)
      end

      def jsonapi_resource(*actions)
        before_action :set_resource, only: [:show, :update, :destroy]
        before_action :set_collection, only: [:index]
        before_action :validate_type, only: [:create, :update]
        before_action :process_fields, only: [:show, :index]
        actions.each do |action|
          send(:alias_method, action, "jsonapi_#{action}".to_sym)
        end
      end
    end

    module InstanceMethods

      def paginated?
        (self.class.class_variable_defined?(:@@paginator) && self.class.class_variable_get(:@@paginator)) || false
      end

      # Factorable Actions
      def jsonapi_index
        @collection = apply_filters @collection, params[:filter]
        @collection = apply_sorts   @collection, params[:sort]
        included = self.class.model_included
        included += (params[:include].split(',').map {|rel|rel.underscore}) if params[:include]
        @collection = apply_pagination(@collection, params[:page])
        render json: @collection || [], include: included
      end

      def jsonapi_show
        included = self.class.model_included
        included += (params[:include].split(',').map {|rel|rel.underscore}) if params[:include]
        render json: @resource, include: included
      end

      def jsonapi_create
        @resource = self.class.model_klass.new(jsonapi_params)
        if @resource.save
          render json: @resource, status: :created
        else
          render_error(@resource, :unprocessable_entity)
        end
      end

      def jsonapi_update
        if @resource.update(jsonapi_params)
          render json: @resource, status: :ok
        else
          render_error(@resource, :unprocessable_entity)
        end
      end

      def jsonapi_destroy
        @resource.destroy
        head 204
      end

      private

      def eager_load_relationships(query, included = nil)
        serializer_klass = (self.class.model_klass.to_s + "Serializer").constantize
        serializer_klass._reflections.select do |k, v|
          if (
              v.is_a?(ActiveModel::Serializer::HasOneReflection) ||
              v.is_a?(ActiveModel::Serializer::HasManyReflection) ||
              (included && included.include?(v.to_s))
            )
            query = query.eager_load v.name
          end
        end
        query
      end

      def apply_filters(records, filter_params)
        self.class.model_filters_defaults.merge(filter_params.to_unsafe_hash).each do |filter_name, filter_value|
          next if filter_name.to_sym == :id
          raise JsonApi::Errors::InvalidFilter.new(filter_name) unless self.class.model_filters[filter_name.to_sym]
          records = self.class.model_filters[filter_name.to_sym].call(records, filter_value, self.try(:scope))
        end if filter_params
        records
      end

      def apply_sorts(records, sort_params)
        return records unless sort_params
        sp = sort_params.split(',').map do |sort_param|
          _, direction, sort_param = sort_param.match(/\A([-,+]?)(.*)\Z/).to_a
          direction = (direction.blank? || direction == "+") ? :asc : :desc
          { sort_param.to_s =>  direction }
        end.reduce({}) {|h, kv| h.merge kv }
        sp.merge(self.class.model_sorts_defaults.merge(sp)).each do |sort_param, direction|
          split = sort_param.match(/\A(.*)\[(.*)\]\Z/).to_a
          if split.blank?
            sort_name = sort_param
          else
            sort_name = split[1]
            # TODO VALIDATE sort_id UUID FORMAT
            sort_id = split[2]
          end
          raise JsonApi::Errors::InvalidSort.new(sort_name) unless self.class.model_sorts[sort_name.underscore.to_sym]
          if sort_id
            records = self.class.model_sorts[sort_name.underscore.to_sym].call(records, direction, self.try(:scope), sort_id)
          else
            records = self.class.model_sorts[sort_name.underscore.to_sym].call(records, direction, self.try(:scope))
          end
          end if sort_params
        records
      end

      def jsonapi_params
        @jsonapi_params ||= ActiveModelSerializers::Deserialization.jsonapi_parse(params)
      end

      def apply_pagination(records, paginator)
        return records unless paginated?
        paginator ||= {}
        records.page(paginator[:number].try(:to_i) || 1).per_page([paginator[:size].try(:to_i) || records.per_page, records.per_page].min)
      end

      def set_collection
        if defined?(scope) && self.class.model_klass < ScopedRecord
          @collection = self.class.model_klass.collection(scope)
        else
          @collection = self.class.model_klass.all
        end
        if params[:filter].try(:[], :id)
          @collection = @collection.where(self.class.primary_key => params[:filter].delete(:id).split(','))
        end
        @collection = eager_load_relationships(@collection)
      end

      def set_resource
        set_collection
        @resource = @collection.find(params[:id])
      end

      def process_fields
        if params[:fields]
          params[:fields].each { |k, v| params[:fields][k] = v.split(',') }
        else
          params[:fields] = {}
        end
      end
    end

  end
end