### Imported from my actual company api project
### Developped by redpist and ngouy (me)
### Gem will be released soon

module JsonApi
  class Error < ActionController::BadRequest
    def errors
      {
        errors: @errors,
        jsonapi: {
          version: "1.0"
        }
      }
    end

    def status
      @status
    end

    def to_json
      {
        json: errors, status: status, is_error: true
      }
    end

    protected
    def add_error(err)
      @errors ||= []
      @errors << err
    end
  end

  module Errors

    class InvalidParameterError < Error
      protected
      def initialize(parameter, detail)
        @status = 400
        add_error({
          source: {
            parameter: parameter
          },
          detail: detail,
          title: self.class.to_s
        })
      end
    end

    class InvalidFieldError < Error
      protected
      def initialize(field, detail)
        @status ||= 422
        add_error({
          detail: detail,
          source: {
            # TODO check if relationships or indeed in data
            pointer: "/data/attributes/#{field}"
          },
          title: self.class.to_s
        })
      end
    end

    class FormatError < Error
      protected
      def initialize(title, detail, params)
        @status ||= 422
        add_error(
          detail: detail,
          title: title,
          params: params
        )
      end
    end

    class NotFoundError < Error
      protected
      def initialize(object)
        @status = 404
        add_error({
          details: "#{object} not found"
        })
      end
    end

    class Forbidden < Error
      def initialize(message)
        @status = 403
        add_error(detail: message)
      end
    end

    class Unauthorized < Error
      def initialize(message)
        @status = 401
        add_error(detail: message)
      end
    end

    class PGErrorInvalidValue < InvalidFieldError
      def initialize(parameter, value, status = :unprocessable_entity)
        super parameter, "#{value} is/contains a not valid value"
        @status = status
      end
    end

    class PGErrorNotNullViolation < InvalidFieldError
      def initialize(message)
        super '', message
      end
    end

    class ActionNotFound < NotFoundError
      def initialize(method, controller)
        super("#{method} #{controller}")
      end
    end

    class InvalidFilter < InvalidParameterError
      def initialize(filter)
        super "filter[#{filter}]", "invalid filter"
      end
    end

    class InvalidSort < InvalidParameterError
      def initialize(sort)
        super "sort[#{sort}]", "invalid sort"
      end
    end

    class ParameterMissing < InvalidParameterError
      def initialize(error)
        super "parameters", error.message
      end
    end

    class TypeMismatch < InvalidParameterError
      def initialize(actual_type, waiting_type)
        super "type", "#{actual_type} should be #{waiting_type}"
      end
    end

    class ParametersNotAllowed < InvalidParameterError
      def initialize(param)
        super param, "Not allowed"
        @status = 403
      end
    end

    class InvalidFieldValue < InvalidFieldError
      def initialize(param, value)
        super param, "#{value} is not a valid value for this field"
      end
    end

    class RecordInvalid < InvalidFieldError
      def initialize(resource)
        resource.errors.each do |param, error|
          super(param, error)
        end
      end
    end

    class RecordNotFound < Error
      def initialize(message)
        @status = 404
        add_error(detail: message)
      end
    end

    class WrongPassword < Error
      def initialize(remaining_login_tries)
        @status = 401
        add_error({
          detail: 'wrong_ids',
          meta: {
            param_error: remaining_login_tries
          }
        })
      end
    end

    class MaxRetriesReached < Error
      def initialize(wait_remaining)
        @status = 401
        add_error({
          detail: 'no_tries_left',
          meta: {
            param_error: wait_remaining
          }
        })
      end
    end
  end
end
