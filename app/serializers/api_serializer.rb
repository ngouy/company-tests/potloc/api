class ApiSerializer < ActiveModel::Serializer

  attributes :created_at,
             :updated_at
end
