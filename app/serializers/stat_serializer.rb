class StatSerializer < ApiSerializer
  attributes :stock_up,
             :stock_down,
             :incomes,
             :expenses,
             :transactions_in,
             :transactions_out,
             :start_date,
             :end_date
end