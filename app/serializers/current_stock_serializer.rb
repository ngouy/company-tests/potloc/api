class CurrentStockSerializer < ApiSerializer
  attributes :shoe_name,
             :store_name,
             :stock
end