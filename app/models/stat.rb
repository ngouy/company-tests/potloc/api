class Stat
  include ActiveModel::Serialization


  attr_accessor :filters,
                :logs,
                :stock_up,
                :stock_down,
                :incomes,
                :expenses,
                :transactions_in,
                :transactions_out,
                :start_date,
                :end_date,
  # for serialization only
                :id
  attr_reader   :created_at,
                :updated_at

  def initialize(params = {})
    self.id = SecureRandom::uuid
    extract_filters(params[:filters] || {})
    logs_base
    filter_logs
    extract_stats
  end


  private

  def extract_filters(filters)
    self.filters = {
      shoe_names:  filters[:shoe_names].try(:split, ','),
      store_names: filters[:store_names].try(:split, ',')
    }
    self.start_date = extract_date(filters[:start_date]) || SalesLog.order(:created_at).first.created_at
    self.end_date   = extract_date(filters[:end_date])   || SalesLog.order(:created_at).last.created_at
  end


  def logs_base
    self.logs = SalesLog.where(
      'created_at BETWEEN ? AND ?',
      start_date,
      end_date
    )
  end

  def filter_logs
    %i[shoe_names store_names].each do |key|
      values = filters[key]
      next if values.blank?
      self.logs = logs.where(:"#{key.to_s.singularize}" => values)
    end
  end

  def extract_stats
    self.stock_up,
    self.stock_down,
    self.expenses,
    self.incomes,
    self.transactions_in,
    self.transactions_out =
    logs.pluck(
      <<-EOSQL
        COALESCE(SUM(CASE WHEN stock_diff > 0 THEN stock_diff ELSE 0 END),   0),
        COALESCE(SUM(CASE WHEN stock_diff < 0 THEN stock_diff ELSE 0 END),   0),
        COALESCE(SUM(CASE WHEN stock_diff > 0 THEN balance_diff ELSE 0 END), 0),
        COALESCE(SUM(CASE WHEN stock_diff < 0 THEN balance_diff ELSE 0 END), 0),
        COUNT(CASE WHEN stock_diff        > 0 THEN 42 ELSE NULL END),
        COUNT(CASE WHEN stock_diff        < 0 THEN 42 ELSE NULL END)
      EOSQL
    ).first
  end

  # TODO : Handle many dates type
  def extract_date(date)
    date && Time.strptime(date.to_s, "%s")
  end
end
