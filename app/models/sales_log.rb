class SalesLog < ApplicationRecord
  validates :stock, :store_name, :shoe_name, :balance_diff, :stock_diff, presence: true

  def self.catch_event!(event)
    ActiveRecord::Base.transaction do
      new_log = init_log_from_event(event)
      new_log.init_stock_diff(event)
      new_log.init_balance_diff(event)
      CurrentStock.upsert_from_log!(new_log)
    end
  rescue ActiveRecord::RecordInvalid => e
    error_id = SecureRandom.uuid
    Rails.logger.error("FATAL: #{Time.now} | #{error_id} -- Unable to insert websocket retrived event, params: #{event}")
    Rails.logger.error("#{error_id} -- #{e.message}")
  end

  def self.init_log_from_event(event)
    new(
      store_name:   event['store'],
      shoe_name:    event['model'],
      stock:        event['inventory']
    )
  end

  def init_balance_diff(event)
    self.balance_diff = -stock_diff * ShoeCurrentPrice.find_price!(
      event['model'],
      stock_diff.negative? ? :sell : :buy
    )
  end

  def init_stock_diff(event)
    self.stock_diff = event['inventory'] - (CurrentStock.find_by(
      store_name:  event['store'],
      shoe_name: event['model']
    ).try(:stock) || 0)
  end
end
