class ShoeCurrentPrice < ApplicationRecord
  PRECISION = 2

  alias_attribute :sell, :selling_price
  alias_attribute :buy,  :buying_price

  def self.find_price!(model, direction)
    (find_by(shoe_name: model) || create_with_random!(shoe_name: model))
      .send(direction)
  end

  # for lisibility ONLY, but questionable
  def selling_price=(params)
    super(params.round(PRECISION))
  end

  def buying_price=(params)
    super(params.round(PRECISION))
  end

  # for exercice purpose, set them into seed for more serious usage
  def self.create_with_random!(params)
    new_price = new(params)
    new_price.selling_price = rand * 100 + 50
    new_price.buying_price  = rand * (new_price.selling_price - 40) + 40
    new_price.save!
    new_price
  end

end
