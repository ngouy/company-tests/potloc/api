class CurrentStock < ApplicationRecord
  belongs_to :sales_log

  validates :stock, :store_name, :shoe_name, presence: true

  def self.upsert_from_log!(log)
    find_or_initialize_by(
      store_name: log.store_name,
      shoe_name:  log.shoe_name
    ).update_attributes!(
      stock: log.stock,
      sales_log: log
    )
  end
end
