class ApplicationController < ActionController::API
  ActionController::Serialization

  def not_found
    error = JsonApi::Errors::ActionNotFound.new(request.request_method, request.fullpath.split('?')[0])
    render json: error.errors.to_json, status: error.status
  end
end
