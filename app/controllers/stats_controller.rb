class StatsController < ApiController
  include JsonApi::ControllerMixin::Base

  jsonapi_resource :show

  def set_resource
    @resource = Stat.new(filters: params[:filter])
  end

end