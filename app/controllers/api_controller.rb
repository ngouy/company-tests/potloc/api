class ApiController < ApplicationController
  include ActiveSupport::Rescuable

  rescue_from JsonApi::Error do |e|
    render_jsonapi_error e
  end

  rescue_from ActionController::ParameterMissing do |e|
    render_jsonapi_error JsonApi::Errors::ParameterMissing.new(e)
  end

  rescue_from ActiveRecord::RecordInvalid do |e|
    render_jsonapi_error JsonApi::Errors::RecordInvalid.new(@resource)
  end

  rescue_from ActionController::InvalidAuthenticityToken do |e|
    render_jsonapi_error JsonApi::Errors::Unauthorized.new("your api token doesn't allowed you to access/modify this data")
  end

  rescue_from ActiveRecord::RecordNotFound do |e|
    if (e.model == "ApiKey" || e.model == "AccountApiKey" || e.model == "AppApiKey")
      render_jsonapi_error JsonApi::Errors::Unauthorized.new("Your ApiKey doesn't exists anymore")
    else
      render_jsonapi_error JsonApi::Errors::RecordNotFound.new(e.message)
    end
  end

  rescue_from PG::InvalidTextRepresentation do |e, f, g|
    splited = e.message.split("\n")[0].split(':')
    render_jsonapi_error JsonApi::Errors::PGErrorInvalidValue.new(
      splited[1].split(' ')[-1],
      splited[2][2..-2],
      ["index", "show"].include?(params["action"]) ? 400 : :unprocessable_entity
    )
  end

  rescue_from PG::NotNullViolation do |e|
    render_jsonapi_error JsonApi::Errors::PGErrorNotNullViolation.new(e.message.split('DETAIL')[0][8..-2])
  end

  private

  def render_jsonapi_error error
    render json: error.errors, status: error.status, is_error: true#, adapter: nil, serializer: ActiveModel::Serializer::Null
  end
end