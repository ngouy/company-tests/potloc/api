require 'faye/websocket'
require 'eventmachine'
require 'json'
require ::File.expand_path('../../config/environment',  __FILE__)
Rails.application.eager_load!

run ActionCable.server

EM.run {
  ws = Faye::WebSocket::Client.new('ws://localhost:8182/')

  ws.on :message do |event|
    SalesLog.catch_event!(JSON.parse(event.data))
  end
}