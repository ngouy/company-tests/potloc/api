ActiveModelSerializers.config.adapter = :json_api
ActiveModelSerializers.config.jsonapi_include_toplevel_object = true
ActiveModelSerializers.config.serializer_lookup_enabled = true
ActiveModelSerializers.config.jsonapi_pagination_links_enabled = false
JsonApi::Error