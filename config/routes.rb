Rails.application.routes.draw do

  # dasherize ressources path
  def jsonapi_resources(*args)
    args[1]            ||= {}
    args[1][:path]     ||= "/#{args.first.to_s.dasherize}"
    resources(*args)
  end

  scope 'api/' do
    jsonapi_resources :current_stocks
    jsonapi_resources :stats
  end

  # handle every not defined routes
  %w(get post patch put delete).each do |verb|
    self.send(verb, '', to: 'application#not_found')
    self.send(verb, '*path', to: 'application#not_found')
  end
end
